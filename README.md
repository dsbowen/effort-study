# Effort study

[![pipeline status](https://gitlab.com/dsbowen/effort-study/badges/master/pipeline.svg)](https://gitlab.com/dsbowen/effort-study/-/commits/master)
[![coverage report](https://gitlab.com/dsbowen/effort-study/badges/master/coverage.svg)](https://gitlab.com/dsbowen/effort-study/-/commits/master)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Feffort-study/HEAD?urlpath=lab/tree/examples)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## Open in Gitpod

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/dsbowen/effort-study)

## Deploy to heroku

If you're not logged in already, log in with

``` bash
$ heroku login -i
```

Deploy the app using

``` bash
$ bash deploy.sh
```

Edit `deploy.sh` to configure the database plan, Redis plan, and server scale and type.

For small pilots, I use

- heroku-postgresql:hobby-basic
- redistogo:micro
- ps:type standard-1x
- ps:scale web=5 worker=1

## Run the app

### With a mock redis queue

``` bash
$ python app.py
```

### With redis in Docker

Start a worker listening on the redis server.

``` bash
$ export REDIS_PORT=6000
$ docker pull redis
$ docker run -d -p $REDIS_PORT:6379 redis
$ export REDIS_URL=redis://0.0.0.0:$REDIS_PORT
$ python worker.py
```
Run the app in a separate terminal tab.

``` bash
$ export REDIS_PORT=6000
$ export REDIS_URL=redis://0.0.0.0:$REDIS_PORT
$ python app.py redis
```

To run with `gunicorn`, replace `python app.py redis` with

``` bash
$ gunicorn --worker-class eventlet -w 1 app:app
```

