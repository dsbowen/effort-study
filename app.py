import os
import sys

from hemlock import create_app, socketio
from hemlock_ax import init_app, init_test_app

import src

app = create_app()

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "redis":
        from rq.job import Job

        init_app(app)
        for job_id in app.ax_queue.finished_job_registry.get_job_ids():
            Job.fetch(job_id, connection=app.ax_queue.connection).delete()
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(os.getcwd(), "data.db")
    else:
        init_test_app(app)
    
    socketio.run(app, debug=True)
else:
    init_app(app)
