$(document).ready(function() {
    var points_input = $("#{{ points_input.hash }}");
    var payment_input = $("#{{ payment_input.hash }}");
    var task_input = $("#{{ task_input.hash }}");
    var points_span = $("#points");
    var payment_span = $("#payment");
    var next_press_span = $("#next-press");

    task_input.on("input", function() {
        if ($(this).val() == next_press_span.text()){
            if ($(this).val() == "a") {
                next_press_span.text("b");
            }
            else {
                next_press_span.text("a");

                points = parseInt(points_input.val()) + 1;
                points_input.val(points);
                points_span.text(points);

                payment = Math.floor({{ cents }} * points / {{ per_points }});
                payment_input.val(payment);
                payment_span.text(payment);
            }
        }
        $(this).val("");
    })
})