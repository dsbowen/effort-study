from hemlock import create_test_app
from hemlock_ax import init_test_app, run_test

import src

N_USERS = 100


def test():
    app = create_test_app()
    init_test_app(app)
    run_test(N_USERS)
