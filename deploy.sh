#!/bin/bash
read -p "Enter an app name: " NAME
read -sp "Enter a password: " PASSWORD
heroku apps:create $NAME
heroku config:set PASSWORD=$PASSWORD > /dev/null
heroku addons:create heroku-postgresql:hobby-dev
heroku addons:create redistogo:nano
git add .
git commit -m "Ready to deploy."
git push heroku master
heroku ps:type free
heroku ps:scale web=1 worker=1