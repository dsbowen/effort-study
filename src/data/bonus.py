"""Compute bonuses.

.. code-block:: bash

    $ python path/to/bonus.py study-name

Replace ``study-name`` with the name of the study, e.g., ``pilot00``.
"""
import os
import sys

import pandas as pd

DATA_FOLDER = "data"

if __name__ == "__main__":
    study_name = sys.argv[1]
    df = pd.read_csv(os.path.join(os.getcwd(), DATA_FOLDER, f"{study_name}.csv"))
    df = df[df.mturk_id != "test"]
    columns = ["mturk_id", "payment"]
    if "completion_code" in df:
        columns.append("completion_code")
        
    completed_df = df[(df.completed & ~df.errored)][columns]
    completed_df["payment"] /= 100
    completed_df.to_csv(
        os.path.join(os.getcwd(), DATA_FOLDER, f"{study_name}_completed.csv"), index=False
    )
    df[df.errored]["mturk_id"].to_csv(
        os.path.join(os.getcwd(), DATA_FOLDER, f"{study_name}_errored.csv"), index=False
    )
