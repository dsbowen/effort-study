"""Extract the data as a csv file from a postgres database.

First, you will need to set the following environment variables.

.. code-block:: bash

    $ export PGUSER=my-postgres-username PGPASSWORD=my-postgres-password DBNAME=my-database-name

Usually, ``my-postgres-username`` is ``postgres``.

Then, run this file.

.. code-block:: bash

    $ python path/to/extract.py my-csv-name.csv

Your csv file will be in ``data/my-csv-name.csv`` in the current working directory.
"""
import os
import sys

import pandas as pd
from hemlock import User, create_test_app
from hemlock.app import Config

DATA_FOLDER = "data"

if __name__ == "__main__":
    data_path = os.path.join(os.getcwd(), DATA_FOLDER)
    if not os.path.exists(data_path):
        os.mkdir(data_path)

    app = create_test_app(
        Config(),
        {
            "SQLALCHEMY_DATABASE_URI": f"postgresql://{os.getenv('PGUSER')}:{os.getenv('PGPASSWORD')}@localhost/{os.getenv('DBNAME')}"
        },
    )
    df = pd.concat([user.get_data(use_cached_data=False) for user in User.query.all()])
    # df = User.get_all_data(refresh_if_in_progress=True)
    df.to_csv(os.path.join(data_path, sys.argv[1]), index=False)
