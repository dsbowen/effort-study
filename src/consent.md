**IMPORTANT.** This study uses experimental software, and you may encounter an error. If you do, please contact me, Dillon Bowen, at dsbowen@wharton.upenn.edu. Please tell me the last page you remember seeing and what you clicked on or typed on that page. Your user experience is important to me, and I will do my best to correct any errors and compensate you fairly for your time and effort. Thank you in advance for your patience.

**Please take this study on a laptop or desktop, *not* on a mobile device.**

**Study purpose.** This study is about how people respond to incentives.

**Non-participation statement.** Your participation is voluntary. You can withdraw from the study at any time by closing the browser window. You must be at least 18 years old to participate.

**Compensation.** You will be paid if and only if you complete the study. We will not compensate you if you answer questions randomly or fail to read the instructions.

**Procedures.** We will pay you based on your performance in a simple task. This study should take 1-12 minutes to complete.

**Confidentiality.** All of your responses are confidential. We will not ask for personally identifiable information, such as your name, although we will ask you general demographics questions.