"""Survey navigation.
"""
from __future__ import annotations

import os

import numpy as np
import pandas as pd
from flask import render_template
from flask_login import current_user
from hemlock import User, Page
from hemlock.functional import compile, validate
from hemlock.questions import Check, Input, Label
from hemlock.utils.format import plural
from hemlock.utils.random import make_hash
from hemlock_ax.assign import Assigner, get_data as super_get_data
from sqlalchemy_mutable.utils import partial

if os.getenv("FLASK_ENV") == "development":
    N_TREATMENTS = 2
    MIN_USERS_PER_CONDITION = 2
else:
    N_TREATMENTS = 60
    MIN_USERS_PER_CONDITION = 5
    
# assumed maximum number of points humanly possible to score based on pilots
# a higher score is likely due to cheating (e.g., a key-pressing script)
MAX_POINTS = 4000
PRACTICE_TIME = 30 * 1000  # milliseconds
TASK_TIME = 10 * 60 * 1000  # milliseconds
NEEDLE = (1, 100)
HAYSTACK = (0, 100)
FILE_DIR = os.path.dirname(os.path.abspath(__file__))


def get_data(assigner: Assigner) -> pd.DataFrame:
    """Get data to fit the model.
    
    Exclude users who entered "test" as their MTurk ID or get an impossibly high score.

    Arguments:
        assigner (Assigner): Adaptive assigner.

    Returns:
        pd.DataFrame: Data.
    """
    if not len(df := super_get_data(assigner)):
        return df
    return df[(df.mturk_id.str.lower() != "test") & (df.target < MAX_POINTS)]


assigner = Assigner(
    {"treatment_index": list(range(N_TREATMENTS))},
    get_data=get_data,
    min_users_per_condition=MIN_USERS_PER_CONDITION
)
# treatments is a list of (cents, per points) tuples
# the zeroeth treatment is the "needle" with a higher payment per points scored
# the rest give idental payments
treatments = [NEEDLE] + (N_TREATMENTS - 1) * [HAYSTACK]


@User.route("/survey")
def seed() -> list[Page]:
    """Seeds the survey with a consent page.

    Returns:
        list[Page]: Consent and demographics pages.
    """
    current_user.meta_data["completion_code"] = make_hash(6)
    return [
        Page(
            Label(open(os.path.join(FILE_DIR, "consent.md"), "r").read()),
            mturk_input := Input(
                "Please enter your MTurk ID.",
                floating_label="MTurk ID",
                variable="mturk_id",
                validate=validate.require_response("Please enter your MTurk ID."),
                test_response="test-mturk-id",
            ),
            Input(
                "Please confirm your MTurk ID.",
                floating_label="MTurk ID",
                validate=validate.compare_response(
                    mturk_input, "MTurk IDs must match."
                ),
                test_response="test-mturk-id",
            ),
        ),
        Page(
            Input(
                "How old are you?",
                variable="age",
                input_tag={"type": "number", "min": 0},
            ),
            Check(
                "What is your gender?", ["Male", "Female", "Other"], variable="gender"
            ),
            navigate=main_survey_branch,
        ),
    ]


def main_survey_branch(root: Page = None) -> list[Page]:
    """Create the main survey branch.

    This branch includes instructions, a practice round, and the main task.

    Args:
        root (Page, optional): The consent page. Defaults to None.

    Returns:
        list[Page]: Main survey branch.
    """

    def make_task_page(practice):
        """Create the task page.

        Args:
            practice (bool): Indicates that this is the practice round.

        Returns:
            Page: Task page.
        """
        page = Page(
            points_input := Input(
                default=0,
                variable=None if practice else "target",
                input_tag={"type": "number"},
                test_response=partial(random_points, cents, per_points),
                extra_html_settings={"card": {"style": {"display": "none"}}},
            ),
            payment_input := Input(
                default=0,
                variable=None if practice else "payment",
                input_tag={"type": "number"},
                test_response=0,
                extra_html_settings={"card": {"style": {"display": "none"}}},
            ),
            task_input := Input(
                f"""
                Remaining time: <span id='clock-id'></span>

                **{bonus_info}**

                Points: <span id='points'>0</span>

                Bonus payment: <span id='payment'>0</span> cents

                Press **<span id='next-press'>a</span>**
                """,
                compile=compile.auto_advance(
                    PRACTICE_TIME if practice else TASK_TIME, clock_id="clock-id"
                ),
            ),
            timer="practice_time" if practice else "task_time",
        )
        task_input.html_settings["js"].append(
            render_template(
                "task.js",
                points_input=points_input,
                payment_input=payment_input,
                task_input=task_input,
                cents=cents,
                per_points=per_points,
            )
        )
        return page

    treatment_index = assigner.assign_user()["treatment_index"]
    cents, per_points = treatments[int(treatment_index)]

    if cents > 0:
        bonus_info = f"You will be paid an extra {cents} {plural(cents, 'cent')} for every {per_points} points you score."
    else:
        bonus_info = "Your score will not affect your payment in any way."

    return [
        Page(
            Label(
                f"""
                {open(os.path.join(FILE_DIR, "instructions.md"), "r").read()}

                You can practice the task for up to 30 seconds on the next page.

                **{bonus_info}**
                """
            )
        ),
        make_task_page(practice=True),
        Page(Label("On the next page, you will perform the task for real.")),
        task_page := make_task_page(practice=False),
        Page(Label(compile=partial(display_bonus, task_page))),
    ]


def display_bonus(bonus_label: Label, task_page: Page) -> None:
    """Display the user's bonus.

    Args:
        bonus_label (Label): Label on which to display the bonus.
        task_page (Page): Page on which the user performed the task.
    """
    # Note: response may be None if participant is taking the study on certain mobile devices
    points_input, payment_input = task_page.questions[:2]
    bonus_label.label = f"""
        You scored {int(points_input.response or 0)} points and earned a
        {int(payment_input.response or 0)} cent bonus.

        Your completion code is **{current_user.get_meta_data()["completion_code"]}**
    """


def random_points(question: Input, cents: int, per_points: int) -> int:
    """Generate random points based on rough values from DellaVigna and Pope.

    Args:
        question (Input): Points input.
        cents (int): Payment scale.
        per_points (int): Number of points to get the payment.

    Returns:make
        int: Points scored.
    """
    if (cents, per_points) == NEEDLE:
        points = int(1200 + 300 * np.random.normal())
    else:
        points = int(800 + 300 * np.random.normal())
    return max(points, 0)
